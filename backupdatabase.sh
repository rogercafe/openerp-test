#! /bin/bash
exec > /tmp/myLog.log 2>&1
set -x

FILE_NAME=`echo "$(date +%F--%T)_$FAZZO_CLIENT_NAME.sql.gz"`
echo "Preparando para gerar arquivo $FILE_NAME"
pg_dump -Uopenerp polygames | gzip -c > $FILE_NAME
echo "Arquivo de backup gerado, preparando para enviar arquivo para nuvem"
/usr/local/bin/aws s3 cp $FILE_NAME s3://fazzo/testopenerp/databasebackup/$FILE_NAME
echo "Arquivo enviado com sucesso!!!"